
Rese�a:


Este proyecto fue una gran reto para m�, ya que el front-end es mi debilidad sin embargo no hay escusas uno siempre debe esforzarse a dar lo mejor de uno porque el que se deja dominar por flojeras, escusas y decirse que uno no puede hacer algo no es palabra de ganador si no de perdedor.

La maquetaci�n web es algo incre�ble y m�s cuando sientes que te gusta tu interfaz, yo en mi caso nunca me ha gustado  mi nivel desarrollo front-end si no el backend. Siempre he sido una persona dedicada a pensar en la l�gica y no he tenido una capacidad para el mundo de manera art�stica sin embargo me gusto trabajar en este proyecto porque me hiso pensar bastante a las soluciones, me hiso aprender m�s a fondo  el maquetado web y sobre todo aplicar mis conocimientos en POO en algo tan complejo  como la animaci�n del DOM.



Portal educativo dirigido los alumnos del Instituto Universitario Jes�s Obrero con la finalidad de que puedan interactuar y aprender sobre el desarrollo web.

El proyecto contara con la teor�a necesaria para aprender desarrollo web, acompa�ado con teor�as complementarias sobre: Metodolog�as de desarrollo de Software y Ciclo de vida del software.

Este proyecto est� realizado por el estudiante de 5to semestre Christian Moreno que con esfuerzo y experiencia muestra la informaci�n para los alumnos de la carrera de inform�tica de Instituto Universitario Jes�s Obrero.

Instalaci�n: 

Se deber� tener Conocimiento del sistema de control de versiones git y obtendr� este proyecto mediante el uso del comando git clone a trav�s del siguiente enlace:

(ssh)
git@gitlab.com:iw-2021-1/desarrollo-web.git

(HTTPS)
https://gitlab.com/iw-2021-1/desarrollo-web.git
